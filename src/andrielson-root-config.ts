import { registerApplication, start } from "single-spa";
import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";
import { HTMLLayoutData } from "single-spa-layout/dist/types/isomorphic/constructRoutes";
import microfrontendLayout from "./microfrontend-layout.html";
import externalUrls from "./external-urls.json";
import internalUrls from "./internal-urls.json";
import loading from "./loading.html";

const environment = localStorage.getItem("environment") || "internal";
const environmentUrls =
  environment === "internal" ? internalUrls : externalUrls;

const deploymentUrls = {
  urlMfeAuthentication: "http://localhost:4200/",
};

const data: HTMLLayoutData = {
  props: { deploymentUrls, environmentUrls },
  loaders: {
    loading,
  },
};

const routes = constructRoutes(microfrontendLayout, data);
const applications = constructApplications({
  routes,
  loadApp({ name }) {
    return System.import(name);
  },
});
const layoutEngine = constructLayoutEngine({ routes, applications });

applications.forEach(registerApplication);
layoutEngine.activate();
start();
// System.import("@angular/core").then(a => {
//   a.enableProdMode();
//   start({
//     urlRerouteOnly: true,
//   });
// });